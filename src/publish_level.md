# Publish Level(Scene)

![Publish To Workshop Image](./images/PublishToWorkshop.png)

If you are ready with your level, you can publish your level to the public [Official Hero Jumper Steam Workshop](https://steamcommunity.com/app/2153600/workshop/).

To publish your level, click on the `File` menu on the toolbar of the editor, then on `Publish Level`. A menu called `Public level to workshop` with show. There you can enter the information about your workshop item.

> *Note:* When you choose tags, atleast one of the tags should be chosen.

Make sure you placed `preview.png` as said in [Level File Structure](./level_file_structure.md). If you placed the image while the editor is running, you should click the refresh button to refresh the preview image on the screen.

When you are ready with everything, click the `Publish` button at the bottom of the popup.

After some seconds you can go to the workshop and you will see your item there.
