# `checkInputKey`

## Definition

``` lua
checkInputKey(keyName)
```

## Parameters

- `keyName` - Key's name to check for. Type: string

## Returns

- `bool` - If the key is pressed and hold

## Description

Checks if key is pressed and repeats it until its released.
