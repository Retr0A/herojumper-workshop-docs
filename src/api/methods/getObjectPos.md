# `getObjectPos`

## Definition

``` lua
getObjectPos(objectName)
```

## Parameters

- `objectName` - The name of the object to get. Type: string

## Returns

- `vector3` - the position of the object

## Description

Gets the position of object by name.
