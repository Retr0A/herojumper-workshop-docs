# `createVector3`

## Definition

``` lua
createVector3(x, y, z)
```

## Parameters

- `x` - X Axis of the vector. Type: float
- `y` - Y Axis of the vector. Type: float
- `z` - Z Axis of the vector. Type: float

## Returns

- `vector3` - The created vector 3

## Description

Creates vector3 that can be later used in many functions.
