# `throwError`

## Definition

``` lua
throwError(message)
```

## Parameters

- `message` - Message to print. Type: string

## Description

Prints error message to the consle.

> *Note*: Use this only while editing. Make sure you remove this function before publishing to public.
