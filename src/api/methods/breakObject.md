# `breakObject`

## Definition

``` lua
breakObject(objectName)
```

## Parameters

- `objectName` - Object's name to destroy. Type: string

## Description

Destroys object.
