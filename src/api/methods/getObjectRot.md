# `getObjectRot`

## Definition

``` lua
getObjectRot(objectName)
```

## Parameters

- `objectName` - The name of the object to get. Type: string

## Returns

- `vector3` - the rotation(eulers) of the object

## Description

Gets the rotation in eulers of object by name.
