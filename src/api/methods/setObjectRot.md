# `setObjectRot`

## Definition

``` lua
setObjectRot(objectName, newRot)
```

## Parameters

- `objectName` - The name of the object to set position. Type: string
- `newRot` - The new rotation(eulers) to be set. Type: vector3

## Description

Sets the rotation(eulers) of object by name.
