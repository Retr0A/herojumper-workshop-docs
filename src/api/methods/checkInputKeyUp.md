# `checkInputKeyUp`

## Definition

``` lua
checkInputKeyUp(keyName)
```

## Parameters

- `keyName` - Key's name to check for. Type: string

## Returns

- `bool` - If the key is released

## Description

Checks if key is released. If it is released, then returns true, otherwise false.
