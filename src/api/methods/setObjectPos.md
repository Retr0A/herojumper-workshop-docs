# `setObjectPos`

## Definition

``` lua
setObjectPos(objectName, newPos)
```

## Parameters

- `objectName` - The name of the object to set position. Type: string
- `newPos` - The new position to be set. Type: vector3

## Description

Sets the position of object by name.
