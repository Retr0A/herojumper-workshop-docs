# `addObjectRot`

## Definition

``` lua
addObjectRot(objectName, rotation)
```

## Parameters

- `objectName` - Object's name to rotate. Type: string
- `rotation` - the rotation(eulers) to add. Type: vector3

## Description

Rotate object by eulers.
