# `random`

## Definition

``` lua
random(min, max)
```

## Parameters

- `min` - The range of minimum(inclusive) number to generate. Type: int
- `max` - The range of maximum(inclusive) number to generate. Type: int

## Returns

- `vector3` - the rotation(eulers) of the object

## Description

Generates random number between `min` and `max`
