# `addObjectPos`

## Definition

``` lua
addObjectPos(objectName, position)
```

## Parameters

- `objectName` - Object's name to add position to. Type: string
- `position` - the position to add: vector3

## Description

Moves object by location.
