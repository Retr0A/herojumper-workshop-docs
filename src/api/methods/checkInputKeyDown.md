# `checkInputKeyDown`

## Definition

``` lua
checkInputKeyDown(keyName)
```

## Parameters

- `keyName` - Key's name to check for. Type: string

## Returns

- `bool` - If the key is pressed

## Description

Checks if key is pressed. If it is pressed, then returns true, otherwise false.
