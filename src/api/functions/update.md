# `update`

## Definition

``` lua
function update(deltaTime)
```

## Parameters

- `deltaTime` - The deltaTime when updating. it is inherited from `Time.deltaTime` in Unity, C#

## Description

It is used for defining functions in at *updating*. Code is executed at every single frame after the game started and the start method is executed.
