# `start`

## Definition

``` lua
function start()
```

## Description

It is used for defining functions in beggining. Code is executed at the very start of the game.
