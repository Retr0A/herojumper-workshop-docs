# Scripting

The most fun part of Hero Jumper Level Editor is the scripting.

> It's good to have basic knowledge at Lua before going any further. You can go to the documentation of Lua [here](https://www.lua.org/docs.html).

As said in [Level File Structure](./level_file_structure.md), one of the files in the folder is `main.lua`. This file is containing functions for additional Level Modding.

Example code can be found below:

``` lua
function start() -- Start is called in the first frame of the game
    print('Hello World!'); -- Print "Hello World!" to the console
end

function update(deltaTime) -- Update is called every frame since the start of the game
    if checkInputKeyDown('R') then
        print('The key \"R\" is pressed!'); -- If the key R is pressed, print this message to the console
    end
end
```

You can also see the scripting API for functions [here](./api/functions.md) or for methods [here](./api/methods.md).
