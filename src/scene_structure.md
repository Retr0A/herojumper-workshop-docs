# Scene Structue

Every scene has file in the folder of the level called `map.json`. This file is readable and if you open it, you will see the content of the scene.

The content of the scene is looking like this:

- `sceneName` - The name of the scene.
- `sceneObjects` - List of al objects in the scene.
