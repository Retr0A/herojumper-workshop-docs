# Basic Functions

You can use these basic tools to manage, quit and use the editor properly.

You can find them in the toolbar menus.

| Name             | Description                                                       |
| ---------------- | ----------------------------------------------------------------- |
| Play             | Opens Scene for testing in normal Hero Jumper baseplate map       |
| Save             | Saves current scene to its `map.json` file                        |
| Publish          | Shows publish to workshop popup ([More here](./publish_level.md)) |
| New Object       | Creates new object in the scene                                   |
| Duplicate Object | Copies, then pastes the selected object in the scene              |
| Delete Object    | Deletes the selected object in the scene                          |
| Quit(Editor)     | Quits the editor and goes to the main menu.                       |
