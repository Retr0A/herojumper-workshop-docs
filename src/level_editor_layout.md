# Level Editor Layout

The Level Editor includes 2 panels and 1 toolbar.

![Level Editor Layout Image](images/LevelEditorLayout.png)

1. Toolbar - The toolbar is used for main functions in the editor.
    - File - Used for commands about the level that is currently loaded.
    - Scene - Used for managing the scene (ex. Add Object; Duplicate Object).
    - Editor - Used for controlling the editor (ex. Quit Editor).
2. Scene Objects Panel - This panel is used for managing and outlining every object placed in the opened scene
3. Object Properties Panel - In this panel you can see the selected object's properties like position, rotation, etc.
