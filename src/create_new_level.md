# Create New Level

To create new level follow these steps:

![New Level Image](./images/CreateLevel.png)

1. Go to play menu as normally
2. Enter name for the new map
3. Click the green *"CREATE NEW LEVEL"* button as shown in the image above
4. Congratulations, you now created your first level!

For more instructions go to [Level Editor Layout](./level_editor_layout.md)