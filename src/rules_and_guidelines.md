# Rules and Guidelines

*Every time you upload item to the workshop, you agree with these rules:*

> **Do not post any content on Steam containing the following:**
> 1. Porn, inappropriate or offensive content, warez or leaked content or anything else not safe for work
> 2. Discussion of piracy including, but not limited to:
> 3. Cracks
> 4. Key generators
> 5. Console emulators
> 6. Cheating, hacking, game exploits
> 7. Threats of violence or harassment, even as a joke
> 8. Posted copyright material such as magazine scans
> 9. Soliciting, begging, auctioning, raffling, selling, advertising, referrals
> 10. Racism, discrimination
> 11. Abusive language, including swearing
> 12. Drugs and alcohol
> 13. Religious, political, and other "prone to huge arguments" threads
