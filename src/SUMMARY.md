# Summary

[Getting Started](./getting_started.md)
[Rules and Guidelines](./rules_and_guidelines.md)

# Getting Started

- [Create New Level](./create_new_level.md)
- [Level File Structure](./level_file_structure.md)

# Level Editor

- [Level Editor Layout](./level_editor_layout.md)
    - [Scene Objects Panel](./level_editor_panels/sceneobjects_panel.md)
    - [Object Properties Panel](./level_editor_panels/objectproperties_panel.md)
- [Basic Functions](./level_editor_basic_functions.md)
- [Scripting](./level_editor_scripting.md)

# Level Scene

- [What is Scene?](./what_is_scene.md)
- [Scene Structue](./scene_structure.md)
- [Publish Level(Scene)](./publish_level.md)

# Scripting API

- [Functions](./api/functions.md)
    - [start](./api/functions/start.md)
    - [update](./api/functions/update.md)
- [Methods](./api/methods.md)
    - [createVector3](./api/methods/createVector3.md)
	- [setObjectPos](./api/methods/setObjectPos.md)
	- [addObjectPos](./api/methods/addObjectPos.md)
	- [setObjectRot](./api/methods/setObjectRot.md)
    - [addObjectRot](./api/methods/addObjectRot.md)
	- [getObjectPos](./api/methods/getObjectPos.md)
	- [getObjectRot](./api/methods/getObjectRot.md)
	- [checkInputKeyDown](./api/methods/checkInputKeyDown.md)
	- [checkInputKeyUp](./api/methods/checkInputKeyUp.md)
	- [checkInputKey](./api/methods/checkInputKey.md)
	- [random](./api/methods/random.md)
	- [breakObject](./api/methods/breakObject.md)
	- [throwError](./api/methods/throwError.md)
