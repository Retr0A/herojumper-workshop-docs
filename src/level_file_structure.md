# Level File Structure

Every level in Hero Jumper contains its own folder with content in it.

> Here is the file tree of basic Hero Jumper level:

| File/Folder Name                                         | Description                                  |
| -------------------------------------------------------- | -------------------------------------------- |
| Documents Folder                                         | Documents folder in your Operating System    |
| &ensp; Hero Jumper Maps                                  | Folder for every Hero Jumper Map             |
| &ensp;&ensp;&ensp; My Level                              | Level folder with its content                |
| &ensp;&ensp;&ensp;&ensp;&ensp; .vscode                   | Visual Studio Code settings                  |
| &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp; settings.json | Visual Studio Code settings file             |
| &ensp;&ensp;&ensp;&ensp;&ensp; main.lua                  | The main Lua file that is used for scripting |
| &ensp;&ensp;&ensp;&ensp;&ensp; map.json                  | JSON file containing the scene of the level  |
| &ensp;&ensp;&ensp;&ensp;&ensp; preview.png               | Preview Image for Steam Workshop             |
