# What is Scene?

Every Hero Jumper Level contains one scene. A scene is structure of propertis like `Level Name`, `Level Objects`, etc. The scene is loaded by the editor and later when published, converts to level so it is playable from the consumer.

You can head over to [Scene Structure](./scene_structure.md) for more information.
